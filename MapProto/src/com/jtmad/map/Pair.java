package com.jtmad.map;

import java.util.List;

import com.google.android.gms.maps.model.LatLng;

public class Pair {
	public String name;
	public LatLng latlng;
	public String Distancetext;
	public double Durationvalue;
	public String Durationtext;
	List<LatLng> latlngs;

	Pair(String n, LatLng L, String dist_t, String dur_t, double dur_v, List<LatLng> LLs) {
		name = n;
		latlng = L;
		Distancetext = dist_t;
		Durationtext = dur_t;
		Durationvalue = dur_v;
		latlngs = LLs;
	}
}
