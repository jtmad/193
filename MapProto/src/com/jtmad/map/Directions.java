package com.jtmad.map;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.maps.android.PolyUtil;

public class Directions {
	public DirectionsResult dd;
	public List<LatLng> latlngs;
	Context context;

	public String getDistanceText() {
		return dd.routes.get(0).legs.get(0).distance.text;
	}
	
	public List<LatLng> getLatlngs() {
		return latlngs;
	}
	
	public double getDurationValue() {
		return dd.routes.get(0).legs.get(0).duration.value;
	}
	Directions(Context c) {
		context = c;
		latlngs = new ArrayList<LatLng>();
	}
	
	public String getDurationText() {
		return dd.routes.get(0).legs.get(0).duration.text;
	}

	public GenericUrl buildUrl(LatLng origin, LatLng dest, String mode) {
		String o = String.valueOf(origin.latitude) + ","
				+ String.valueOf(origin.longitude);
		String d = String.valueOf(dest.latitude) + ","
				+ String.valueOf(dest.longitude);
		String googleAPIKey = "AIzaSyCuYDguhtVkRCfzuN4mRwF5m2BjMUgNErk";
		GenericUrl url = new GenericUrl(
				"https://maps.googleapis.com/maps/api/directions/json");
		url.put("origin", o);
		url.put("mode", mode);
		url.put("destination", d);
		url.put("sensor", "false");
		url.put("key", googleAPIKey);
		return url;
	}

	private void getDirection(LatLng origin, LatLng dest, String mode) {
		final HttpTransport HTTP_TRANSPORT = AndroidHttp
				.newCompatibleTransport();
		final JsonFactory JSON_FACTORY = new JacksonFactory();
		DirectionsResult dir = null;
		try {
			HttpRequestFactory requestFactory = HTTP_TRANSPORT
					.createRequestFactory(new HttpRequestInitializer() {
						@Override
						public void initialize(HttpRequest request) {
							request.setParser(new JsonObjectParser(JSON_FACTORY));
						}
					});
			HttpRequest request = requestFactory.buildGetRequest(buildUrl(
					origin, dest, mode));
			HttpResponse httpResponse = request.execute();
			dir = httpResponse.parseAs(DirectionsResult.class);
			String encodedPath = dir.routes.get(0).overview_polyline.points;
			latlngs = PolyUtil.decode(encodedPath);
			dd = dir;
		} catch (Exception e) {
			toast(e.getMessage());
		}
	}

	public void update(LatLng origin, LatLng dest, String mode) {
		getDirection(origin, dest, mode);

	}

	private void toast(String s) {
		Toast.makeText(context, s, Toast.LENGTH_LONG).show();
	}

	private void printConsole(String s) {
		Log.v("eey", s);
	}
}
