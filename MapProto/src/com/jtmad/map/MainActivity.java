package com.jtmad.map;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.ui.IconGenerator;

public class MainActivity extends ActionBarActivity {
	Boolean walking = true;
	ArrayList<Pair> info;
	ArrayList<Marker> placesMarkers;
	ArrayList<Marker> pathMarkers;
	Places p;
	Context context;
	private GoogleMap myMap;
	private Marker marker;
	ArrayList<Polygon> myPolygons;
	LatLng mapCenter;
	int flag5, flag10, flag15;
	LocationManager service;
	Criteria criteria = new Criteria();
	String provider;
	Location location = null;
	Button toggle;
	String mode = "walking";
	String radius = "1000";
	Button go;

	Bitmap bitmap;

	private void makeMarkerIcon() {
		IconGenerator icon = new IconGenerator(getApplicationContext());
		icon.setStyle(2);
		icon.setRotation(230);
		icon.setContentRotation(45);
		bitmap = icon.makeIcon("our location");
	}

	public void toggleButton(View v) {
		if (mode.equals("walking")) {
			mode = "driving";
			toggle.setText("driving");
			radius = "16000";
		} else if (mode.equals("driving")) {
			mode = "walking";
			toggle.setText("walking");
			radius = "1000";
		}
	}
	
	public void goButton(View v) {
		Intent intent = new Intent(this, Anime.class);
	    startActivity(intent);
	    go.setVisibility(View.GONE);
	}

	private void overridePLACES() {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
	}

	private void fillmap() {
		info = p.getInfo();
		for (int i = 0; i < info.size(); i++) {
			float color = BitmapDescriptorFactory.HUE_ORANGE;
			placesMarkers.add(myMap.addMarker(new MarkerOptions()
					.icon(BitmapDescriptorFactory.defaultMarker(color))
					.title(info.get(i).name).position(info.get(i).latlng)));
		}
	}

	private void deleteMarkers() {
		for (Marker m : placesMarkers) {
			m.remove();
		}
		placesMarkers.clear();
		// myMap.clear();
	}

	@SuppressWarnings("unused")
	private void addMarkersToMap(List<LatLng> latLng) {
		deleteMarkers();
		for (LatLng L : latLng) {
			Marker marker = myMap.addMarker(new MarkerOptions().position(L)
					.title("path"));
			pathMarkers.add(marker);
		}
	}

	ArrayList<Polyline> polylines;

	private void drawPath(List<LatLng> latLng) {
		PolylineOptions polys = new PolylineOptions();
		for (LatLng L : latLng) {
			polys.add(L);
		}
		polylines.add(myMap.addPolyline(polys));
		latlngs = latLng;
	}

	private void updatePlaces(String type) {
		p = new Places(context);
		p.update(marker.getPosition(), type, radius, marker.getPosition(), mode);
	}

	private void updateDirections() {
		if (info.size() != placesMarkers.size())
			debug("you fucked up, info size != marker size");

		for (int i = 0; i < info.size(); i++) {
			Pair currentInfo = info.get(i);
			Marker currentPlaceMarker = placesMarkers.get(i);

			// set marker info
			currentPlaceMarker.setSnippet(currentInfo.Distancetext + " "
					+ currentInfo.Durationtext);

			// set color based on duration 5min, 5 to 10min, 10min+
			float color;
			double duration = currentInfo.Durationvalue;
			if (duration <= 300)
				color = BitmapDescriptorFactory.HUE_GREEN;
			else if (duration > 300 && duration <= 600)
				color = BitmapDescriptorFactory.HUE_YELLOW;
			else
				color = BitmapDescriptorFactory.HUE_RED;
			currentPlaceMarker.setIcon(BitmapDescriptorFactory
					.defaultMarker(color));
		}
	}

	private void selectType() {
		CharSequence array[] = { "half mile", "5 miles", "10miles" };
		new AlertDialog.Builder(this).setTitle("options")
				.setItems(array, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							break;
						case 1:

							break;
						case 2:
							break;
						default:
							break;
						}
					}
				}).setIcon(android.R.drawable.ic_dialog_alert).show();
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		deleteLines();
		// selectType();
		// myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
		// marker.getPosition(), 13));
		switch (item.getItemId()) {
		case R.id.menu1:
			deleteMarkers();
			updatePlaces("restaurant");
			fillmap();
			updateDirections();
			break;
		case R.id.menu2:
			deleteMarkers();
			updatePlaces("gas_station");
			fillmap();
			updateDirections();
			break;
		case R.id.menu3:
			deleteMarkers();
			updatePlaces("pharmacy");
			fillmap();
			updateDirections();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void debug(String s) {
		Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
	}

	private void doMapStuff() {
		LatLng traderjoes = new LatLng(38.546814, -121.761541);
		myMap.setMyLocationEnabled(true);
		myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(traderjoes, 15));
		makeMarkerIcon();
		marker = myMap.addMarker(new MarkerOptions()
				.title("MyLocation")
				.snippet("contour starts here")
				.position(traderjoes)
				.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
				.draggable(true));
		
	}

	private void checkMapLoaded() {
		if (myMap == null) {
			myMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			if (myMap != null) {
				doMapStuff();
			}
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		polylines = new ArrayList<Polyline>();
		overridePLACES();
		placesMarkers = new ArrayList<Marker>();
		pathMarkers = new ArrayList<Marker>();
		service = (LocationManager) getSystemService(LOCATION_SERVICE);
		boolean enabled = service
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (!enabled) {
			// debug("debug");
		} else
			provider = service.getBestProvider(criteria, false);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		checkMapLoaded();
		toggle = (Button) findViewById(R.id.toggle);
		go = (Button) findViewById(R.id.animate);
		myPolygons = new ArrayList<Polygon>();
		service = (LocationManager) getSystemService(LOCATION_SERVICE);
		myMap.setOnCameraChangeListener(new OnCameraChangeListener() {

			@Override
			public void onCameraChange(CameraPosition pos) {
				LatLng coord = pos.target;
				mapCenter = coord;
			}
		});
		myMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {

			@Override
			public boolean onMyLocationButtonClick() {
				Location location;

				boolean enabled = service
						.isProviderEnabled(LocationManager.GPS_PROVIDER);
				if (!enabled)
					debug("debug");
				else {
					location = service.getLastKnownLocation(provider);
					if (location != null) {
						marker.setPosition(new LatLng(location.getLatitude(),
								location.getLongitude()));
					} else
						debug("Waiting for location");
				}
				mapCenter = marker.getPosition();
				myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapCenter,
						15));
				return false;
			}
		});
		context = this;
		myMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker m) {
				deleteLines();
				for (int i = 0; i < placesMarkers.size(); i++) {
					if (placesMarkers.get(i).equals(m))
						drawPath(info.get(i).latlngs);
				}
				go.setVisibility(View.VISIBLE);
				return false;
			}
		});

	}
	public static List<LatLng> latlngs;
	private void deleteLines() {
		for (Polyline p : polylines) {
			p.remove();
		}
		polylines.clear();
	}
}
