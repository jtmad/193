package com.jtmad.map;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class Anime extends ActionBarActivity {

	private List<Marker> markers = new ArrayList<Marker>();

	private GoogleMap googleMap;
	private final Handler mHandler = new Handler();

	private Marker selectedMarker;

	Handler handler = new Handler();
	Random random = new Random();
	Runnable runner = new Runnable() {
		@Override
		public void run() {
		}
	};

	public void startButton(View v) {
		animator.startAnimation(true);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.anime_layout);
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map2);
		googleMap = mapFragment.getMap();
		handler.postDelayed(runner, random.nextInt(2000));
		googleMap.setMyLocationEnabled(true);
		googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

			@Override
			public void onMapClick(LatLng latLng) {
				addMarkerToMap(latLng);
			}

		});
		addMarkersToMap(MainActivity.latlngs);
		LatLng traderjoes = new LatLng(38.546814, -121.761541);
		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(traderjoes, 15));
	}

	private Animator animator = new Animator();

	int currentPt;

	CancelableCallback MyCancelableCallback = new CancelableCallback() {

		@Override
		public void onCancel() {
			System.out.println("onCancelled called");
		}

		@Override
		public void onFinish() {

			if (++currentPt < markers.size()) {

				float targetBearing = bearingBetweenLatLngs(
						googleMap.getCameraPosition().target,
						markers.get(currentPt).getPosition());

				LatLng targetLatLng = markers.get(currentPt).getPosition();
				// float targetZoom = zoomBar.getProgress();

				System.out.println("currentPt  = " + currentPt);
				System.out.println("size  = " + markers.size());
				// Create a new CameraPosition
				CameraPosition cameraPosition = new CameraPosition.Builder()
						.target(targetLatLng)
						.tilt(currentPt < markers.size() - 1 ? 90 : 0)
						.bearing(targetBearing)
						.zoom(googleMap.getCameraPosition().zoom).build();

				googleMap.animateCamera(
						CameraUpdateFactory.newCameraPosition(cameraPosition),
						3000, MyCancelableCallback);
				System.out.println("Animate to: "
						+ markers.get(currentPt).getPosition() + "\n"
						+ "Bearing: " + targetBearing);

				markers.get(currentPt).showInfoWindow();

			} else {
				// info.setText("onFinish()");
			}

		}

	};

	public class Animator implements Runnable {

		private static final int ANIMATE_SPEEED = 300;
		private static final int ANIMATE_SPEEED_TURN = 300;
		private static final int BEARING_OFFSET = 20;

		private final Interpolator interpolator = new LinearInterpolator();

		int currentIndex = 0;

		float tilt = 90;
		float zoom = 15.5f;
		boolean upward = true;

		long start = SystemClock.uptimeMillis();

		LatLng endLatLng = null;
		LatLng beginLatLng = null;

		boolean showPolyline = false;

		private Marker trackingMarker;

		public void reset() {
			resetMarkers();
			start = SystemClock.uptimeMillis();
			currentIndex = 0;
			endLatLng = getEndLatLng();
			beginLatLng = getBeginLatLng();

		}

		public void stop() {
			trackingMarker.remove();
			mHandler.removeCallbacks(animator);

		}

		public void initialize(boolean showPolyLine) {
			reset();
			this.showPolyline = showPolyLine;

			highLightMarker(0);

			if (showPolyLine) {
				polyLine = initializePolyLine();
			}

			// We first need to put the camera in the correct position for the
			// first run (we need 2 markers for this).....
			LatLng markerPos = markers.get(0).getPosition();
			LatLng secondPos = markers.get(1).getPosition();

			setupCameraPositionForMovement(markerPos, secondPos);

		}

		private void setupCameraPositionForMovement(LatLng markerPos,
				LatLng secondPos) {

			float bearing = bearingBetweenLatLngs(markerPos, secondPos);

			trackingMarker = googleMap.addMarker(new MarkerOptions()
					.position(markerPos));

			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(markerPos)
					.bearing(bearing + BEARING_OFFSET)
					.tilt(90)
					.zoom(googleMap.getCameraPosition().zoom >= 16 ? googleMap
							.getCameraPosition().zoom : 16).build();

			googleMap.animateCamera(
					CameraUpdateFactory.newCameraPosition(cameraPosition),
					ANIMATE_SPEEED_TURN, new CancelableCallback() {

						@Override
						public void onFinish() {
							System.out.println("finished camera");
							animator.reset();
							Handler handler = new Handler();
							handler.post(animator);
						}

						@Override
						public void onCancel() {
							System.out.println("cancelling camera");
						}
					});
		}

		private Polyline polyLine;
		private PolylineOptions rectOptions = new PolylineOptions();

		private Polyline initializePolyLine() {
			// polyLinePoints = new ArrayList<LatLng>();
			rectOptions.add(markers.get(0).getPosition());
			return googleMap.addPolyline(rectOptions);
		}

		/**
		 * Add the marker to the polyline.
		 */
		private void updatePolyLine(LatLng latLng) {
			List<LatLng> points = polyLine.getPoints();
			points.add(latLng);
			polyLine.setPoints(points);
		}

		public void stopAnimation() {
			animator.stop();
		}

		public void startAnimation(boolean showPolyLine) {
			if (markers.size() > 2) {
				animator.initialize(showPolyLine);
			}
		}

		@Override
		public void run() {
			long elapsed = SystemClock.uptimeMillis() - start;
			double t = interpolator.getInterpolation((float) elapsed
					/ ANIMATE_SPEEED);
			double lat = t * endLatLng.latitude + (1 - t)
					* beginLatLng.latitude;
			double lng = t * endLatLng.longitude + (1 - t)
					* beginLatLng.longitude;
			LatLng newPosition = new LatLng(lat, lng);
			trackingMarker.setPosition(newPosition);
			if (showPolyline) {
				updatePolyLine(newPosition);
			}
			if (t < 1) {
				mHandler.postDelayed(this, 16);
			} else {
				if (currentIndex < markers.size() - 2) {
					currentIndex++;
					endLatLng = getEndLatLng();
					beginLatLng = getBeginLatLng();
					start = SystemClock.uptimeMillis();
					LatLng begin = getBeginLatLng();
					LatLng end = getEndLatLng();
					float bearingL = bearingBetweenLatLngs(begin, end);
					highLightMarker(currentIndex);
					CameraPosition cameraPosition = new CameraPosition.Builder()
							.target(end)
							// changed this...
							.bearing(bearingL + BEARING_OFFSET).tilt(tilt)
							.zoom(googleMap.getCameraPosition().zoom).build();
					googleMap.animateCamera(CameraUpdateFactory
							.newCameraPosition(cameraPosition),
							ANIMATE_SPEEED_TURN, null);
					start = SystemClock.uptimeMillis();
					mHandler.postDelayed(animator, 16);
				} else {
					currentIndex++;
					highLightMarker(currentIndex);
					stopAnimation();
				}
			}
		}

		private LatLng getEndLatLng() {
			return markers.get(currentIndex + 1).getPosition();
		}

		private LatLng getBeginLatLng() {
			return markers.get(currentIndex).getPosition();
		}

	};

	/**
	 * Allows us to navigate to a certain point.
	 */
	public void navigateToPoint(LatLng latLng, float tilt, float bearing,
			float zoom, boolean animate) {
		CameraPosition position = new CameraPosition.Builder().target(latLng)
				.zoom(zoom).bearing(bearing).tilt(tilt).build();
		changeCameraPosition(position, animate);
	}

	public void navigateToPoint(LatLng latLng, boolean animate) {
		CameraPosition position = new CameraPosition.Builder().target(latLng)
				.build();
		changeCameraPosition(position, animate);
	}

	private void changeCameraPosition(CameraPosition cameraPosition,
			boolean animate) {
		CameraUpdate cameraUpdate = CameraUpdateFactory
				.newCameraPosition(cameraPosition);
		if (animate) {
			googleMap.animateCamera(cameraUpdate);
		} else {
			googleMap.moveCamera(cameraUpdate);
		}
	}

	private Location convertLatLngToLocation(LatLng latLng) {
		Location loc = new Location("someLoc");
		loc.setLatitude(latLng.latitude);
		loc.setLongitude(latLng.longitude);
		return loc;
	}

	private float bearingBetweenLatLngs(LatLng begin, LatLng end) {
		Location beginL = convertLatLngToLocation(begin);
		Location endL = convertLatLngToLocation(end);
		return beginL.bearingTo(endL);
	}

	public void toggleStyle() {
		if (GoogleMap.MAP_TYPE_NORMAL == googleMap.getMapType()) {
			googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		} else {
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		}
	}

	/**
	 * Adds a marker to the map.
	 */
	public void addMarkerToMap(LatLng latLng) {
		Marker marker = googleMap.addMarker(new MarkerOptions()
				.position(latLng).title("title").snippet("snippet"));
		markers.add(marker);
	}

	public void addMarkersToMap(List<LatLng> latLngs) {
		Marker marker = null;
		for (LatLng latLng : latLngs) {
			marker = googleMap.addMarker(new MarkerOptions().position(latLng)
					.title("title").snippet("snippet"));
			marker.setVisible(false);
			markers.add(marker);
		}
		marker.setVisible(true);
	}

	/**
	 * Clears all markers from the map.
	 */
	public void clearMarkers() {
		googleMap.clear();
		markers.clear();
	}

	/**
	 * Remove the currently selected marker.
	 */
	public void removeSelectedMarker() {
		this.markers.remove(this.selectedMarker);
		this.selectedMarker.remove();
	}

	/**
	 * Highlight the marker by index.
	 */
	private void highLightMarker(int index) {
		highLightMarker(markers.get(index));
	}

	/**
	 * Highlight the marker by marker.
	 */
	private void highLightMarker(Marker marker) {
		marker.setIcon(BitmapDescriptorFactory
				.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
		this.selectedMarker = marker;
	}

	private void resetMarkers() {
		for (Marker marker : this.markers) {
			marker.setIcon(BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_RED));
		}
	}

}