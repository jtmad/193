package com.jtmad.map;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;

public class Places {
	private Context context;
	private List<Pp> places;
	private ArrayList<Pair> info;

	public Places(Context c) {
		context = c;
		info = new ArrayList<Pair>();
	}

	public ArrayList<Pair> getInfo() {
		return info;
	}

	public GenericUrl buildUrl(String type, LatLng coord, String radius) {
		String coo = String.valueOf(coord.latitude) + ","
				+ String.valueOf(coord.longitude);
		String googleAPIKey = "AIzaSyCuYDguhtVkRCfzuN4mRwF5m2BjMUgNErk";
		GenericUrl url = new GenericUrl(
				"https://maps.googleapis.com/maps/api/place/search/json");
		url.put("types", type);
		url.put("location", coo);
		url.put("radius", radius);
		url.put("sensor", "false");
		url.put("key", googleAPIKey);
		return url;
	}

	private void getPlacesList(LatLng coord, String type, String radius) {
		final HttpTransport HTTP_TRANSPORT = AndroidHttp
				.newCompatibleTransport();
		final JsonFactory JSON_FACTORY = new JacksonFactory();
		Plist placesResult = null;
		try {
			HttpRequestFactory requestFactory = HTTP_TRANSPORT
					.createRequestFactory(new HttpRequestInitializer() {
						@Override
						public void initialize(HttpRequest request) {
							request.setParser(new JsonObjectParser(JSON_FACTORY));
						}
					});
			HttpRequest request = requestFactory.buildGetRequest(buildUrl(type,
					coord, radius));
			HttpResponse httpResponse = request.execute();
			placesResult = httpResponse.parseAs(Plist.class);
			places = placesResult.results;
		} catch (Exception e) {
			toast(e.getMessage());
		}
	}

	public void update(LatLng coord, String type, String radius,
			LatLng currentPosition, String mode) {
		getPlacesList(coord, type, radius);
		for (Pp p : places) {
			LatLng ll = new LatLng(p.geometry.location.lat,
					p.geometry.location.lng);

			// get Direction data
			Directions d = new Directions(context);
			d.update(currentPosition, ll, mode);

			info.add(new Pair(p.name, ll, d.getDistanceText(), d
					.getDurationText(), d.getDurationValue(), d.getLatlngs()));
		}
	}

	private void toast(String s) {
		Toast.makeText(context, s, Toast.LENGTH_LONG).show();
	}

	private void printf(String s) {
		Log.v("eey", s);
	}

}
