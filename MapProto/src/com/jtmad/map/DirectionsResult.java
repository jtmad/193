package com.jtmad.map;

import java.util.List;

import com.google.api.client.util.Key;

public class DirectionsResult {
	@Key
	public List<Route> routes;

	public static class Route {
		@Key
		public overview_polyline overview_polyline;

		@Key("copyrights")
		public String copyrights;
		
		@Key("legs")
		public List<Legs> legs;
	}

	public static class Legs {
		@Key("distance")
		public Distance distance;
		
		@Key("duration")
		public Duration duration;
		
	}
	
	public static class Distance {
		@Key("text")
		public String text;
		@Key("value")
		public double value;
	}
	
	public static class Duration {
		@Key("text")
		public String text;
		@Key("value")
		public double value;
	}

	public static class overview_polyline {
		@Key
		public String points;
	}

	@Key
	public String status;

}